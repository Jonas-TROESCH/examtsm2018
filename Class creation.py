#comments to explain the code are present on the "Simulation" file.
#This file correspond only to the question 2: creation of the class customer and test of the order and tell methods.
import random
#from Exploratory import Time,Proba_Drinks,Proba_Food

menuD=[("frappucino",2,0.1295718323218211),
	   ("soda",2,0.306283125156213),
	   ("coffee",1,0.17250075302652576),
	   ("tea",1,0.12995315213699316),
	   ("water",1,0.13110672468709345),
	   ("milkshake",3,0.1305844126713536)]

menuF=[("sandwich",3,0.41774771868094407),
   ("pie",5,0.19485721590805324),
   ("muffin",2,0.19349359891397647),
   ("cookie",1,0.19390146649702622)]

class Customer(object):
	lastID=0
	def __init__(self, returning, regular):
		self.id = Customer.lastID
		Customer.lastID+=1
		self.returning = returning
		self.regular = regular
		if not returning:
			self.budget=100
		else:
			if regular:
				self.budget=250
			else:
				self.budget=500
		self.histD=[]
		self.histF=[]
	
	def description(self):
		if self.returning:
			if self.regular:
				c_type="Regular Client"
			else:
				c_type="Hipster"
		else:
			if self.regular:
				c_type="Onetimer"
			else:
				c_type="Tripadvisor"
		print("CID : "+ str(self.id) +" ("+c_type+")")
	
	def order(self):
		if self.budget==0 or (not self.returning and len(self.histD)+len(self.histF)>=1):
			return
		r= random.random()
		choise=0
		while r> menuD[choise][2]:
			r-=menuD[choise][2]
			choise+=1
		if menuD[choise][1]<=self.budget:
			self.budget-=menuD[choise][1]
			self.histD.append(menuD[choise])
			r= random.random()
			choise=0
			while r> menuF[choise][2]:
				r-=menuF[choise][2]
				choise+=1
			if menuF[choise][1]<=self.budget:
				self.budget-=menuF[choise][1]
				self.histF.append(menuF[choise])

	def tell(self):
		print ("Client "+ str(self.id)+" ordered :")
		totalD=0
		totalF=0
		for i in range(len(menuD)):
			count= self.histD.count(menuD[i])
			if count!=0:
				order=menuD[i][0]
				price=menuD[i][1]
				print ("	"+str(count)+" "+order+"(s) : "+str(count)+"x"+str(price)+" = "+str(count*price))
				totalD+=count*price
		for i in range(len(menuF)):
			count= self.histF.count(menuF[i])
			if count!=0:
				order=menuF[i][0]
				price=menuF[i][1]
				print ("	"+str(count)+" "+order+"(s) : "+str(count)+"x"+str(price)+" = "+str(count*price))
				totalF+=count*price
		print ("	in total he payed "+ str(totalD+totalF) +" : "+str(totalD)+" for drinks and "+str(totalF)+" for food")
		

#-----------------------------------------------------------
c0 = Customer(True, False)
c1 = Customer(True, True)
c1.order()
c1.order()
c1.order()
c1.tell()

#----------------------------------------------------------------------------------------



