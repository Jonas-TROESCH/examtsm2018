import pandas as pd
import numpy as np
import os

#To check the method desired, just delete the "#" before each print

#Load CSV
Load_csv = pd.read_csv("./data/Coffeebar_2013-2017.csv", sep=';', header=0, skipfooter=1, engine='python')

#print(Load_csv)
#print (Load_csv.columns.values)

#Extract Each Column
Food        = Load_csv['FOOD']
Drinks      = Load_csv.iloc[:,2]
Customers   = Load_csv.iloc[:,1]
Time        = Load_csv.iloc[:,0]

# ucalculate unique food and drinks and costumers
Food_type   = pd.unique(Food)
Food_type   = Food_type[~pd.isnull(Food_type)]

Drinks_type = pd.unique(Drinks)
Drinks_type  = Drinks_type[~pd.isnull(Drinks_type)]

Cust_unique = Customers.drop_duplicates(keep='first', inplace=False)

#Prin the Outcomes
'''
print("Food Served in Coffee Bar:")
print(Food_type)
print("Drinks Served in Coffee Bar:")
print(Drinks_type)
print("Number of Customers in the bar:")
print(len(Cust_unique))
'''
#count the number of times a certain food is ordered in the 5 years
list_food=[]
for i in range(len(Food_type)):
    list_food.append(Food.str.count(Food_type[i]).sum())
#print(list_food)

#count the number of times a certain drink is ordered in the 5 years
list_drinks=[]
for i in range(len(Drinks_type)):
    list_drinks.append(Drinks.str.count(Drinks_type[i]).sum())
#print(list_drinks)

#GRAPHS

import matplotlib.pyplot as plt
import matplotlib

#figure of Foods
fig = plt.figure(1)
plt.plot(Food_type, list_food, 'g+')
plt.xlabel('Foods')
plt.ylabel('list foods')
plt.title('plot1')
plt.grid(True)
plt.show()


#figure Of drinks

fig = plt.figure(2)
plt.plot(Drinks_type, list_drinks, 'g+')
plt.xlabel('Drinks')
plt.ylabel('list Drinks')
plt.title('plot2')
plt.grid(True)
plt.show()


# Probability of having each drink at each time
Time = pd.to_datetime(Time)
HourCount  = Load_csv.groupby(Time.dt.strftime("%H:%M"))['DRINKS'].count()
HourFilter = Load_csv.groupby(Time.dt.strftime("%H:%M"))['DRINKS'].value_counts()
Proba_Drinks = pd.DataFrame(HourFilter/HourCount)

# Series to dataframe
drink_proba =Proba_Drinks.iloc[:,0].values.tolist()
Proba_Drinks.rename(index=str, columns={'DRINKS': 'Proba'}, inplace=True)
Proba_Drinks.reset_index(inplace=True)
drink_times =pd.unique(Proba_Drinks.iloc[:,0].values.tolist())
drinks_list=[]
for i in range(len(drink_times)):
    drinks_list.append((drink_times[i],drink_proba[i*6:i*6+6]))
#print(Proba_Drinks)

# Probability of having each drink at each time
Time = pd.to_datetime(Time)
HourCount1  = Load_csv.groupby(Time.dt.strftime("%H:%M"))['FOOD'].count()
HourFilter1 = Load_csv.groupby(Time.dt.strftime("%H:%M"))['FOOD'].value_counts()
Proba_Food = pd.DataFrame(HourFilter1/HourCount1)

# Series to datafram
food_proba =Proba_Food.iloc[:,0].values.tolist()
Proba_Food.rename(index=str, columns={'FOOD': 'Proba'}, inplace=True)
Proba_Food.reset_index(inplace=True)
Food_times =pd.unique(Proba_Food.iloc[:,0].values.tolist())
food_list=[]
for i in range(len(Food_times)):
    food_list.append((Food_times[i],food_proba[i*4:i*4+4]))
#print(Proba_Food)
