import random
import numpy as np
from Exploratory import drinks_list
import matplotlib.pyplot as plt
import matplotlib
import os

# from Exploratory import Time,Proba_Drinks,Proba_Food

menuD = [("frappucino", 2, 0.1295718323218211),
         ("soda", 2, 0.306283125156213),
         ("coffee", 1, 0.17250075302652576),
         ("tea", 1, 0.12995315213699316),
         ("water", 1, 0.13110672468709345),
         ("milkshake", 3, 0.1305844126713536)]

menuF = [("sandwich", 5, 0.41774771868094407),
         ("pie", 3, 0.19485721590805324),
         ("muffin", 3, 0.19349359891397647),
         ("cookie", 2, 0.19390146649702622)]

proba_food = 0.5263783589789601
'''
def timeToStr(time):
    res=""
    if time[1] < 9:
        res += '0' + str(time[1])
    else:
        res += str(time[1])
    res+=':'
    if time[0] < 9:
        res += '0' + str(time[0])
    else:
        res += str(time[0])
    return res
'''


class Customer(object):
    lastID = 0

    #this method define the budget in function of the type of customer
    def __init__(self, returning, regular):
        self.id = Customer.lastID
        Customer.lastID += 1
        self.returning = returning
        self.regular = regular
        if not returning:
            self.budget = 100
        else:
            if regular:
                self.budget = 250
            else:
                self.budget = 500
        #creation of lists to input historic of order for each customer
        self.histD = []
        self.histF = []

    def description(self):
        if self.returning:
            if self.regular:
                c_type = "Regular Client"
            else:
                c_type = "Hipster"
        else:
            if self.regular:
                c_type = "Onetimer"
            else:
                c_type = "Tripadvisor"
        return "Client " + str(self.id) + " (" + c_type + ")"

    def order(self, time):
        debug = ""
        if self.budget == 0 or (not self.returning and len(self.histD) + len(self.histF) >= 1):
            return 0
        r = random.random()
        choise = 0
        '''a=-1
        for d in range(len(drinks_list)):
            if drinks_list[d][0]==timeToStr(time):
                a=d
                break
        if a!=-1:

        while r > drinks_list[a][1][choise]:
            r -= drinks_list[a][1][choise]
            choise += 1'''
        pay = 0
        #decrease r as long as probabily of menuD are superior (to find the product correponding to random probability)
        while r > menuD[choise][2]:
            r -= menuD[choise][2]
            choise += 1
        if menuD[choise][1] <= self.budget:
            self.budget -= menuD[choise][1]
            self.histD.append(menuD[choise])
            pay += menuD[choise][1]
            # debugging
            debug += self.description() + " buys " + menuD[choise][0]

            #same process for food
            r = random.random()
            if r < proba_food and time[1] >= 11:
                choise = 0
                r = random.random()
                while r > menuF[choise][2]:
                    r -= menuF[choise][2]
                    choise += 1
                if menuF[choise][1] <= self.budget:
                    self.budget -= menuF[choise][1]
                    self.histF.append(menuF[choise])
                    pay += menuD[choise][1]
                    debug += " and a " + menuF[choise][0]
        print("[" + str(time[1]) + ":" + str(time[0]) + "] " + debug)
        return pay

#method tell to describe the total paid for drinks and food separately and the budget that left to the customer.
    def tell(self):
        totalD = 0
        totalF = 0
        print("Client " + str(self.id) + " ordered :")

        #loop to run the list menuD and count the number of time each product appears
        for i in range(len(menuD)):
            count = self.histD.count(menuD[i])
            if count != 0:
                order = menuD[i][0]
                price = menuD[i][1]
                print("	" + str(count) + "  " + order + "(s) : " + str(count) + "x" + str(price) + " = " + str(
                    count * price))
                totalD += count * price

        #same for food
        for i in range(len(menuF)):
            count = self.histF.count(menuF[i])
            if count != 0:
                order = menuF[i][0]
                price = menuF[i][1]
                print("	" + str(count) + " " + order + "(s) : " + str(count) + "x" + str(price) + " = " + str(
                    count * price))
                totalF += count * price

        #Total computation
        if totalD + totalF > 0:
            print("	In total he payed " + str(totalD + totalF) + " : " + str(totalD) + " for drinks and " + str(
                totalF) + " for food")
            print("	he has " + str(self.budget) + " left")
        else:
            print("		nothing")
        return totalF + totalD


returningCustomers = 1000
# create the 1000 returning customers
customers = []
for i in range(returningCustomers):
    if i % 3 == 0:
        customers.append(Customer(True, False))
    else:
        customers.append(Customer(True, True))

fig = plt.figure(3)
plt.xlabel('Time (days)')
plt.ylabel('Average Day Flow')
plt.title('Cash Flow')
day = 0
for day in range(365 * 5):
    # simulation for one day
    payday = 0
    for hour in np.arange(8, 17, 1):
        for minute in np.arange(0, 59, 1):
            r = random.random()
            if r <= .2:
                payday += customers[int(r * 5 * returningCustomers)].order((minute, hour))
            else:
                r = random.random()
                c = Customer(False, r > .1)
                c.order((minute, hour))
                customers.append(c)
    plt.plot(day, payday, 'r.')
    day += 1
plt.show()
#save the plot on the destatination required
#fig.savefig(os.path.abspath("../examtsm2018/5ara.png"))

customers[318].tell()
